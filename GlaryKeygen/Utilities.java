import java.util.Random;

public class Utilities
{
	public static void main(String[] args)
	{
		String ca = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789<>";
		String serial = "Y7886167958";

		for (int i = 0; i < 9; i++)
			serial += ca.toCharArray()[new Random().nextInt(ca.length())];

		serial = serial.substring(0, 4) + "-" + serial.substring(4, 8) + "-" + serial.substring(8, 12) + "-" + serial.substring(12, 16) + "-" + serial.substring(16, 20);

		System.out.println("Registration code: " + serial);
	}
}
