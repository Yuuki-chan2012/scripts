import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class Rename
{
	private static Properties config = new Properties();
	private static boolean debug = false;
	private static String in = "";
	private static String out = "";

	public static void main(String[] args) throws IOException
	{
		System.out.println("Rename v0.3 by Yuuki-chan // https://yuuki-chan.xyz/\n");
		initConfig();
		rename();
	}

	private static void initConfig()
	{
		try
		{
			config.load(new FileInputStream("config.properties"));

			in = config.getProperty("inPath");
			out = config.getProperty("outPath");
			debug = Boolean.parseBoolean(config.getProperty("debug"));
		}
		catch (Exception e)
		{
			e.printStackTrace(); // Shouldn't ever happen
		}
	}

	private static void rename()
	{
		File folder = new File(in);
		File[] files = folder.listFiles();

		for (int i = 0; i < files.length; i++)
		{
			if (files[i].isFile())
			{
				if (files[i].getName().endsWith(".m2t"))
				{
					String oldName = files[i].getName();
					String newName = "";
					String fN2 = files[i].getName().replace(".m2t", "");
					String[] fNa = fN2.split("-"); // 0 = ID | 1 = Episode | 2-3 = Useless | 4 = Channel

					File fi = new File(out + oldName + ".aria2");
					if (fi.exists())
					{
						System.out.println("[Info]: File hasn't finished downloading, aborting.");
						System.exit(0);
					}

					fNa[0] = fNa[0].replace(fNa[0], config.getProperty(fNa[0]));
					if (Integer.parseInt(fNa[4]) <= 100)
						fNa[4] = fNa[4].replace(fNa[4], config.getProperty(config.getProperty("pref").toLowerCase() + "." + fNa[4]));
					else
						fNa[4] = fNa[4].replace(fNa[4], config.getProperty(fNa[4]));

					if (Integer.parseInt(fNa[1]) <= 9)
						fNa[1] = "0" + fNa[1];

					newName = fNa[0] + " - " + fNa[1] + " (" + fNa[4] + " " + getDimension(fNa[4]) + " MPEG2 AAC).ts";

					File c = new File(out + newName);
					if (c.exists())
					{
						writeLog("[Info]: " + newName + " exists, not moving.", true);

						if (debug)
							System.out.println("[Info]: " + newName + " exists, not moving.");
					}
					else
					{
						if (files[i].renameTo(c))
						{
							writeLog("[Info]: " + oldName + " => " + newName, true);

							if (debug)
								System.out.println("[Info]: " + oldName + " => " + newName);
						}
						else
						{
							writeLog("[Error]: An error occured trying to move " + oldName + " to " + newName, true);

							if (debug)
								System.out.println("[Error]: An error occured trying to move " + oldName + " to " + newName);
						}
					}
				}
			}
		}
	}

	private static String getDimension(String res)
	{
		if (res.equalsIgnoreCase("BS11")
		 || res.equalsIgnoreCase("WOWOW-Live")
		 || res.equalsIgnoreCase("WOWOW-Prime")
		 || res.equalsIgnoreCase("WOWOW-Cinema")
		 || res.equalsIgnoreCase("KBS")
		 || res.equalsIgnoreCase("TVN")
		 || res.equalsIgnoreCase("BS4")
		 || res.equalsIgnoreCase("BS-NTV")
		 || res.equalsIgnoreCase("BSP")
		 || res.equalsIgnoreCase("NHK-BSP"))
			res = "1920x1080";
		else
			res = "1440x1080";

		return res;
	}

	private static void writeLog(String text, boolean ow)
	{
		BufferedWriter bw = null;
		FileWriter fw = null;

		try
		{
			fw = new FileWriter("Rename.log", ow);
			bw = new BufferedWriter(fw);
			bw.write(text + "\n");
		}
		catch (IOException e)
		{
		}
		finally
		{
			try
			{
				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();
			}
			catch (IOException ex)
			{
			}
		}
	}
}
