import java.math.BigInteger;

public class DGIKeygen
{
	private static final BigInteger EXPONENT = new BigInteger("11AD4FDD63A3E8879153DA4F569AF62817968D05BBC6FA7E6FD62C5811179366B", 16);
	private static final BigInteger MODULO = new BigInteger("1A83F7CC1575DCCB59FDC77701E8713C4DEBABECEA715F1CB42575EFD7B963995", 16);

	public static void main (String[] args)
	{
		if (args.length == 1)
		{
			System.out.println(genKey(args[0]));
		}
	}

	private static String genKey(String crc)
	{
		if (crc.length() == 8)
		{
			if (crc.matches("([[a-fA-F-0-9]]{8})"))
			{
				BigInteger pow = EXPONENT;
				BigInteger mod = MODULO;
				String number = String.format("%s%s%s", "6BBBBBBB", crc, "1047BBBBBBBBBBBBBBBBBBBBBBBBBBBBD89033CC");
				BigInteger num = new BigInteger(number, 16);
				BigInteger out = num.modPow(pow, mod);

				return out.toString(16);
			}
			else
				return "Input value doesn't match CRC pattern, it should match \"[[a-fA-F-0-9]]{8}\"";
		}
		else
			return "Invalid length, CRC should be max 8 characters, no less, no more.";
	}
}
